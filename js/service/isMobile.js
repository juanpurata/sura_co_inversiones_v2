var app = angular.module('sura');

app.factory('BrowserService', ['$window', function($window) {
  var BrowserService = {};

  BrowserService.isMobile = function() {
    return angular.element($window).width() < 768;
  };

  BrowserService.isTablet = function() {
    return (angular.element($window).width() >= 768) && (angular.element($window).width() < 1200);
  };

  return BrowserService;
}]);
