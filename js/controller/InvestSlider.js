var app = angular.module('sura');

var InvestSlider = function ($scope, $element, $window, $location, $rootScope, BrowserService) {

  angular.element(document).ready(function () {
    if (BrowserService.isMobile()) {
      var sliderInvestBlockMobile = jQuery('.InvestBlock__wrapper');
      sliderInvestBlockMobile.slick({
        slidesToScroll: 1,
        slidesToShow: 1,
        draggable: true,
        speed: 800,
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3200,
        waitForAnimate: false,
        pauseOnHover: true,
        centerMode: true,
        prevArrow: "<div class='ArrowSlider Icon__arrow-prev'></div>",
        nextArrow: "<div class='ArrowSlider Icon__arrow-next'></div>"
      });
    }
    else {
      var sliderInvestBlock = jQuery('.InvestBlock__wrapper');
      sliderInvestBlock.slick({
        slidesToScroll: 1,
        slidesToShow: 3,
        draggable: true,
        speed: 800,
        dots: false,
        infinite: false,
        autoplay: true,
        autoplaySpeed: 3200,
        waitForAnimate: false,
        pauseOnHover: true,
        prevArrow: "<div class='ArrowSlider Icon__arrow-prev'></div>",
        nextArrow: "<div class='ArrowSlider Icon__arrow-next'></div>"
      });
    }
  });
};
app.controller('InvestSlider', ['$scope', '$element', '$window', '$location', '$rootScope', 'BrowserService', InvestSlider]);
