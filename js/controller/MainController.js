var app = angular.module('sura');

var MainController = function ($scope, $element, $window, $location, $rootScope, BrowserService) {
  $scope.ready = false;
  if (BrowserService.isMobile()) {
    $element.closest('html').addClass('mobile');
  } else $element.closest('html').addClass('desktop');

  $scope.initSlider = function() {
    angular.element(document).ready(function () {
      var sliderBlock = jQuery('.SliderContent');
      sliderBlock.slick({
        slidesToScroll: 1,
        slidesToShow: 1,
        draggable: true,
        speed: 800,
        dots: false,
        infinite: false,
        autoplay: true,
        autoplaySpeed: 3200,
        waitForAnimate: false,
        pauseOnHover: true,
        appendArrows: $element.find(".SliderBlockContent__arrows-content"),
        prevArrow: "<div class='ArrowSlider Icon__arrow-prev'></div>",
        nextArrow: "<div class='ArrowSlider Icon__arrow-next'></div>"
      });
    });
  };
  $scope.showInfo = false;
  $scope.PopupVar = false;
  $scope.InversionFrame = false;
  $scope.Register = false;
  $scope.showRegisterPopup = function() {
    $scope.PopupVar = true;
    $element.find(".Popup").addClass('active');
    $scope.InversionFrame = false;
    $scope.Register = true;
  };
  $scope.showInversionFramePopup = function() {
    $scope.PopupVar = true;
    $element.find(".Popup").addClass('active');
    $scope.InversionFrame = true;
    $scope.Register = false;
  };
  $scope.showPopup = function() {
    $scope.PopupVar = true;
    $element.find(".Popup").addClass('active');
  };
  $scope.closePopup = function() {
    $scope.PopupVar = false;
    $element.find(".Popup").removeClass('active');
  };
  if (BrowserService.isMobile()) {
    $scope.MenuMobile = function() {
      $element.find(".HeaderContent").toggleClass('active');
    };
  }
};

app.controller('sliderBlock', function($scope, $element, $timeout) {
  jQuery(document).ready(function() {
    $timeout( function(){
      $scope.initSlider();
    }, 100 );
  });
});

app.controller('MainController', ['$scope', '$element', '$window', '$location', '$rootScope', 'BrowserService', MainController]);





